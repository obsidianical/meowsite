---
title: "Setting Up a Blog With Hugo and Nix Flakes"
date: 2023-01-25T13:21:21+01:00
summary: "How I set up this website using Nix Flakes and Hugo"
draft: false
categories: 
- programming
tags:
- hugo
- nix
- flakes
- blog
---

If you can read this, it works I hope! I'm writing this as both a test and a first blog post for this website.

I've decided on Hugo mainly because I got recommended it on the fediverse for a static site generator and heard good things about it.

I'm also using this as a chance to properly set up and learn Nix Flakes.

At the time of writing this I got some basics working and the following `flake.nix` file with it, though it's probably by far not as good as it could be.

```nix
{
  description = "Flake for my personal website";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
  };

  outputs = { self, nixpkgs, flake-utils }: 
    let 
      system = "x86_64-linux";
    in 
    with import nixpkgs {
      system = system;
    }; {
      packages.${system}.default = pkgs.stdenv.mkDerivation {
        buildInputs = [ pkgs.hugo pkgs.libsass];
        name = "website";
        src = self;
        buildPhase = "hugo -d res";
        installPhase = "mkdir -p $out/dist; cp -r res/* $out";
      };
      devShell = pkgs.mkShell {
        buildInputs =  [ pkgs.hugo pkgs.libsass];
      };
    };
}
```

This post is mainly intended as a test, so it's not exactly good.
