---
title: "How to not accidentally track your users with Nginx"
summary: "How to not accidentally track your users when using nginx (read the default configs, be smarter then me)"
date: 2023-01-31T15:28:05+01:00
draft: false
tags:
- nginx
- privacy
---

It appears that I accidentally logged the IP-adress of anyone visiting anything hosted on my webserver, be it my previous temporary site or my vaultwarden instance.

tl;dr: read the default configs, be smarter then me. 

So how do you track someone accidentally? Well, you use Nginx on largely default settings.

When I installed Nginx I missed that part of the configuration defaults include logging which also contained the client IP adresses and user agents, among other things.

To disable the logging, it should be sufficient to replace any `access_log` and `error_log` directives' parameters with `off`, and remove the `log_format` directive. If you, however, just want to reduce the logging, then cutting down the `log_format` should be enough. 

Why should you care? Well, IP adresses are personal data under the GDPR, so you'd have to either inform users of your logging or just not do it at all.

None of this post is legal advice, but I hope that this gets indexed by some search engine and someone setting up a server stumbles upon it and removes the logging or informs their users at least. 

If any of the information contained in this post is incorrect or you feel I should add something, please contact me so I can correct or amend the post!

Meow, <br/>
Schrottkatze.
