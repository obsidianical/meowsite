---
title: "About"
description: "About Schrottkatze..."
date: 2023-01-25T14:09:21+01:00
draft: false
type: "about"
menu: "Navbar"
---

# Who am I?

I'm Schrottkatze, a student and (for now) hobbyist programmer and CG artist.

I use they/she pronouns, in languages that do not have neutral pronouns I prefer feminine pronouns.

## My skillset

### Code things

I know various programming languages including TypeScript, Python and Java, but my main language is, and will continue to be for the forseeable future, Rust.

I'm a quite decent web developer and have experience with HTML (and [Pug](https://pugjs.org), CSS (and [Sass/SCSS](https://sass-lang.com/) and web frameworks such as [Vue](https://vuejs.org/), [Yew](https://yew.rs/), [Nuxt](https://nuxtjs.org/) and the static site generator [Hugo](https://gohugo.io/) which I'm using for this website.

### Design/Art/Computer Graphics

I'm a design student and have experience with [InkScape](https://inkscape.org/), [Scribus](https://www.scribus.net/) and of course [the GNU Image Manipulation Program (GIMP)](https://www.gimp.org/).

My main interest art-wise however lies with 3D computer graphics, mainly using [Blender](https://www.blender.org/) and [Material Maker](https://materialmaker.org/). I've originally started CG with Material Maker, and later learned blender.

I still make quite a lot of materials, [here are some that I published](https://materialmaker.org/materials?count=50&type=material&license_mask=0&author=807) to the Material Maker website!

I occasionally do video editing too, so I have some experience with [Kdenlive](https://kdenlive.org/).

