{
  description = "Flake for my personal website";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
  };

  outputs = { self, nixpkgs, flake-utils }: 
    let 
      system = "x86_64-linux";
    in 
    with import nixpkgs {
      system = system;
    }; {
      packages.${system}.default = pkgs.stdenv.mkDerivation {
        buildInputs = [ pkgs.hugo pkgs.libsass];
        name = "meowsite";
        src = self;
        buildPhase = "hugo -d res";
        installPhase = "mkdir -p $out/dist; cp -r res/* $out";
      };
      devShell = pkgs.mkShell {
        buildInputs =  [ pkgs.hugo pkgs.libsass];
      };
    };
}
