---
title: "Blog on Miau"
description: "Welcome to the Blog of Miau!"
date: 2023-01-25T13:21:21+01:00
summary: "Meowmeowmeow"
draft: false
menu: Navbar
---

Why is this called "Blog on Miau"? Because I named the site "Miau" when creating it with Hugo, and when adding it to my RSS reader to test it, the name was "Blog on Miau", and I like it. Miau.
