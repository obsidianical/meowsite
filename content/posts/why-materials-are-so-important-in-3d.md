---
title: "Why Materials Are So Important in 3d"
date: 2023-02-12T18:50:43+01:00
summary: "How you can make your renders look better"
draft: false
tags:
- 3d
- computergraphics
- materials
---

In this post I'll talk about a topic that has been nagging me for a while now: How important proper PBR materials and (material-)shaders actually are in 3d computer graphics.

I often see 3d artists that completely neglect to make proper materials when creating their models and scenes. Often they just either hack something together with random shaders or put no care in making proper materials. 

As an example, I (and many other 3d artists) often see materials that are completely uniform. What do I mean by that? Well, often they just take a uniform roughness, color or whatever other map. 

I made a little scene with just a Suzanne, a plane and a nishita sky texture to demonstrate.

![A Suzanne blender monkey head with very uniform materials](/images/3d-materials-post/monkey-uniform-tex.png)

As you can see, it looks very artifical. The monkey and the ground both have very uniform materials which look very unnatural.

Now, lets just do the bare minimum: let's add a noise texture in the roughness and color parameters of the principled shader. Nothing fancy, just some variation to make it look slightly smudged and dirty. 

![A Suzanne blender monkey head with slightly noisy materials for the floor and the head](/images/3d-materials-post/monkey-noisy-tex.png)

It's a really, really subtle detail, but it looks a lot more real and fancy. Nothing in the real world, and I mean _nothing_, has a perfectly smooth and uniform surface.

## Adding details to models with materials

Often, you see game devs or 3d modellers using incredibly high poly models to show small details, which solely has the effect of making render times skyrocket or the game unplayable while you could've just made it look amazing with a good normal map.

A great example of a well used normal map to make a model seem significantly more detailed is the _Club Chair_ novelty from the game [Guild Wars 2](https://www.guildwars2.com/). 

![The front of the chair](/images/3d-materials-post/gw-chair-front.png)
![The back of the chair](/images/3d-materials-post/gw-chair-back.png)

Both of these on first (and probably also second glance) look like they're quite detailed models, even though the back is completely flat, the only reason it looks detailed is the great use of normal maps. 

In many cases you'll see CG artists use a super high poly model for that, but even if you wanted it to be realistic you still could just add a few vertices for the shadow of the back. 

## Conclusion?

So, why am I putting in the effort to write a whole post about this? Well, many, if not most 3d artists just say "ooh, materials/shaders aren't _that_ important, I'll just download something and it'll look fiiine", or they just use a really basic material where you'd want something really complex, and then when they try to fix their render feeling off, they put a _lot_ of effort into changing the models and the lighting.

Don't get me wrong, modelling and lighting are, obviously, also very important, they cannot save a render from bad materials. Great materials, however, often can salvage a bad model or bad lighting. 

So, dear artists, when you're making anything 3d, put that extra hour of work in your materials and shaders instead of these extra three into making your models ultra high poly. 

I'll be making more blog posts in the future, talking about how to actually _make_ good materials using blender shaders and, more importantly, [the material and shader authoring tool Material Maker](https://materialmaker.org/).

If you have any criticism for this post, have anything to add to it or disagree with parts (or all?) of it, feel free to talk to me about it! 


